# Docker Challenge

## Dependencias

* Docker
* docker-compose

## Como correr el proyecto

### Ejecutar proyecto

`docker-compose up -d`

### Una vez ejecutado el proyecto puedes acceder a

```
http://localhost/app1

http://localhost/app2
```

### ver los logs de los servicios

`docker-compose logs -f`

### Eliminar proyecto

`docker-compose down`

