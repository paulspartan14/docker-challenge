const express = require('express');
const app = express();
const port = 8080;

const message = process.env.MESSAGE || 'Default message';

app.get('/', (req, res) => {
    res.send('this is the message: ' + message);
});

app.listen(port, () => {
    console.log(`App listening at http://localhost:${port}`);
});